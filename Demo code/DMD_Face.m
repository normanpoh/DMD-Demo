%%
clear all
clc
close all
addpath(genpath(pwd));
%% Read real video video file
%mov = VideoReader('real.mov');
% read spoof
mov = VideoReader('spoof.avi');
%Read all video frames.
nFrames=mov.NumberOfFrames;
for i=1:nFrames
  videoFrame=read(mov,i);
  temp = imresize(double(rgb2gray(videoFrame)),1);
  D(:,i) = temp(:);

end
[rows cols] = size(temp);
%%
D1 = D(:,1:end-1);
D2 = D(:,2:end);
[U,Si,V]=svd(D1,'econ');
A = (U')*(D2)*(V)*(inv(Si));
[EigVec,EigVal] = eig(A); % Compute Eigenvaleus and Eigenvectors of S
omega=log(EigVal); % Compute omega 
Omega=exp(omega);
Psi=U*EigVec; 
%%

[idx] = find(abs(diag(omega))<0.15);
for t=1:2:length(idx)
    figure,
spa = reshape(abs(Psi(:,idx(t))),rows,cols);
imshow(spa,[]);
end

