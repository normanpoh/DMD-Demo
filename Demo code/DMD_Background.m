%%
clear all
clc
close all
%% Read AVI video file

mov = VideoReader('001-bg-01-090.avi');
%Specify that reading should begin 0.6 second from the beginning of the file by setting the CurrentTime property.
%Read all video frames.
nFrames=mov.NumberOfFrames;
for i=1:nFrames
  videoFrame=read(mov,i);
  temp = imresize(double(rgb2gray(videoFrame)),0.5);
  D(:,i) = temp(:);

end
[rows cols] = size(temp);
%%
D1 = D(:,1:end-1);
D2 = D(:,2:end);
[U,Si,V]=svd(D1,'econ');
A = (U')*(D2)*(V)*(inv(Si));
[EigVec,EigVal] = eig(A); % Compute Eigenvaleus and Eigenvectors of S
omega=log(EigVal); % Compute omega 
Omega=exp(omega);
Psi=U*EigVec; 
%%
b=(pinv(Psi))*D1(:,1);
DMDt=(Psi)*(Omega)*(b);
omegaD=abs(diag(omega));
LowRank=exp(min(omegaD));
%DMD=zeros(27648,156);

for t=1:size(D1,2);

 DMD(:,t)=(Psi)*(Omega.^t)*(b);

end

DMDL=(Psi(:,1))*(LowRank)*(b(1,1));

L=zeros(size(D1,1),size(D1,2));
%% Produce the low rank image L (these are the background)
for t=1:size(D1,2);

 L(:,t)=(Psi(:,1))*(LowRank.^t)*(b(1,1));

end

Sr=DMD-abs(L); 

Sr=abs(Sr);

L=abs(L);
%%
clear gcf
%figure('units','normalized','outerposition',[0 0 1 1])
for t=25:100
spa = reshape(abs(Sr(:,t)),rows,cols);
low = reshape(abs(D(:,t)),rows,cols);
imshowpair(low,spa,'montage');
%imshow(reshape(abs(Sr(:,t)),128,104),[]); pause(0.1)
pause(0.1)
end
close all

%%
